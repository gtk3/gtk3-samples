#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

class Equipos:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("xml/main.glade")
        
        self.win_equipos = builder.get_object("win_equipos")
        self.win_equipos.show_all()
        
        self.treeview_equipos = builder.get_object("treeview_equipos")
        
        self.btn_close = builder.get_object("btn_close")
        self.btn_close.connect("clicked", self.on_btn_close_clicked)
        
        self.btn_remove = builder.get_object("btn_remove")
        self.btn_remove.connect("clicked", self.on_btn_remove_clicked)
        
        self.btn_add = builder.get_object("btn_add")
        self.btn_add.connect("clicked", self.on_btn_add_clicked)
        
        self.btn_edit = builder.get_object("btn_edit")
        self.btn_edit.connect("clicked", self.on_btn_edit_clicked)
        
        self.set_columnas_equipos()
        
        self.get_equipos()

    def on_btn_close_clicked(self, button):
        self.win_equipos.hide()
    
    def set_columnas_equipos(self):
        for col in self.treeview_equipos.get_columns():
            self.treeview_equipos.remove_column(col)
     
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview_equipos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_equipos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Puntos", renderer, text=2)
        self.treeview_equipos.append_column(column)

        self.store = Gtk.ListStore(int, str, int)
        self.treeview_equipos.set_model(self.store)
    
    def get_equipos(self):
        db = Database()
        
        sql = """
            select id_equipo, nom_equipo, puntos from equipo
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2]])
    
    def on_btn_remove_clicked(self, button):
        selection = self.treeview_equipos.get_selection()
        model, treeiter = selection.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.win_equipos, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="¿Eliminar este equipo? " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.delete_equipo(model[treeiter][0])
                self.get_equipos()

            dialog.destroy()
    
    def on_btn_add_clicked(self, button):
        form_equipos = EquiposForm(True, self)
    
    def on_btn_edit_clicked(self, button):
        form_equipos = EquiposForm(False, self)
    
    def delete_equipo(self, id_equipo):
        db = Database()
        sql = """
            delete from equipo where id_equipo = %(id_equipo)s
            """
        db.run_sql(sql, {'id_equipo': id_equipo})

class EquiposForm:
    def __init__(self, nuevo, obj_padre):
        self.id_equipo = None
        
        self.obj_padre = obj_padre
        builder = Gtk.Builder()
        builder.add_from_file("xml/main.glade")
        
        self.win_form_equipos = builder.get_object("win_form_equipos")
        
        self.entry_nombre_equipo = builder.get_object("entry_nombre_equipo")
        self.entry_puntaje_equipo = builder.get_object("entry_puntaje_equipo")
        
        self.btn_equipo_cancel = builder.get_object("btn_equipo_cancel")
        self.btn_equipo_cancel.connect("clicked", self.on_btn_equipo_cancel_clicked)
        
        self.btn_equipo_apply = builder.get_object("btn_equipo_apply")
        
        if nuevo:
            self.win_form_equipos.set_title("Nuevo")
            self.btn_equipo_apply.connect("clicked", self.on_btn_equipo_apply_new_clicked)
            self.win_form_equipos.show_all()
            
        else:
            self.win_form_equipos.set_title("Editar")
            self.btn_equipo_apply.connect("clicked", self.on_btn_equipo_apply_edit_clicked)
            
            selection = self.obj_padre.treeview_equipos.get_selection()
            model, treeiter = selection.get_selected()
    
            if treeiter is not None:
                self.id_equipo = model[treeiter][0]
                db = Database()
                
                sql = """
                    select id_equipo, nom_equipo, puntos from equipo where id_equipo = %(id_equipo)s
                    """
                
                result = db.run_select_filter(sql, {'id_equipo': self.id_equipo})
                self.entry_nombre_equipo.set_text(result[0][1])
                self.entry_puntaje_equipo.set_text(str(result[0][2]))
                
                self.win_form_equipos.show_all()
    
    def on_btn_equipo_cancel_clicked(self, button):
        self.win_form_equipos.hide()
    
    def on_btn_equipo_apply_new_clicked(self, button):
        nombre = self.entry_nombre_equipo.get_text()
        puntaje = self.entry_puntaje_equipo.get_text()
        
        db = Database()
        
        sql = """
            insert into equipo (nom_equipo, puntos) values (%(nom_equipo)s, %(puntos)s)
            """
        db.run_sql(sql, {"nom_equipo": nombre, "puntos": puntaje})
        
        self.obj_padre.get_equipos()
        self.win_form_equipos.hide()
    
    def on_btn_equipo_apply_edit_clicked(self, button):
        nombre = self.entry_nombre_equipo.get_text()
        puntaje = self.entry_puntaje_equipo.get_text()
        
        db = Database()
        
        sql = """
            update equipo set nom_equipo = %(nom_equipo)s, puntos = %(puntos)s where id_equipo = %(id_equipo)s
            """
        db.run_sql(sql, {"id_equipo": self.id_equipo, "nom_equipo": nombre, "puntos": puntaje})
        
        self.obj_padre.get_equipos()
        self.win_form_equipos.hide()
