#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

class Jugadores:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("xml/main.glade")
        
        self.win_jugadores = builder.get_object("win_jugadores")
        self.win_jugadores.show_all()
        
        self.treeview_jugadores = builder.get_object("treeview_jugadores")
        
        self.btn_close = builder.get_object("btn_close1")
        self.btn_close.connect("clicked", self.on_btn_close_clicked)
        
        self.btn_remove = builder.get_object("btn_remove1")
        self.btn_remove.connect("clicked", self.on_btn_remove_clicked)
        
        self.btn_add = builder.get_object("btn_add1")
        self.btn_add.connect("clicked", self.on_btn_add_clicked)
        
        self.btn_edit = builder.get_object("btn_edit1")
        self.btn_edit.connect("clicked", self.on_btn_edit_clicked)
        
        self.set_columnas_jugadores()
        
        self.get_jugadores()

    def on_btn_close_clicked(self, button):
        self.win_jugadores.hide()
    
    def set_columnas_jugadores(self):
        for col in self.treeview_jugadores.get_columns():
            self.treeview_jugadores.remove_column(col)
     
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview_jugadores.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_jugadores.append_column(column)
        
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Apellidos", renderer, text=2)
        self.treeview_jugadores.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("id-equipo", renderer, text=3)
        self.treeview_jugadores.append_column(column)
        
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Equipo", renderer, text=4)
        self.treeview_jugadores.append_column(column)

        self.store = Gtk.ListStore(int, str, str, int, str)
        self.treeview_jugadores.set_model(self.store)
    
    def get_jugadores(self):
        db = Database()
        
        sql = """
            select id_jugador, nom_jugador, ape_jugador, jugador.id_equipo, nom_equipo from jugador left join equipo on jugador.id_equipo = equipo.id_equipo
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2], r[3], r[4]])
    
    def on_btn_remove_clicked(self, button):
        selection = self.treeview_jugadores.get_selection()
        model, treeiter = selection.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.win_jugadores, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="¿Eliminar este jugador? " + model[treeiter][1] + " " + model[treeiter][2])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.delete_jugador(model[treeiter][0])
                self.get_jugadores()

            dialog.destroy()
    
    def on_btn_add_clicked(self, button):
        form_jugadores = JugadoresForm(True, self)
    
    def on_btn_edit_clicked(self, button):
        form_jugadores = JugadoresForm(False, self)
    
    def delete_jugador(self, id_jugador):
        db = Database()
        sql = """
            delete from jugador where id_jugador = %(id_jugador)s
            """
        db.run_sql(sql, {'id_jugador': id_jugador})

class JugadoresForm:
    def __init__(self, nuevo, obj_padre):
        self.id_jugador = None
        
        self.obj_padre = obj_padre
        builder = Gtk.Builder()
        builder.add_from_file("xml/main.glade")
        
        self.win_form_jugadores = builder.get_object("win_form_jugadores")
        
        self.entry_nombre_jugador = builder.get_object("entry_nombre_jugador")
        self.entry_apellidos_jugador = builder.get_object("entry_apellidos_jugador")
        
        self.cmbbox_equipos = builder.get_object("cmbbox_equipos")
        renderer = Gtk.CellRendererText()
        self.cmbbox_equipos.pack_start(renderer, False)
        self.cmbbox_equipos.add_attribute(renderer, "text", 0)
        renderer = Gtk.CellRendererText()
        self.cmbbox_equipos.pack_start(renderer, False)
        self.cmbbox_equipos.add_attribute(renderer, "text", 1)
        
        self.store = Gtk.ListStore(int, str)
        self.cmbbox_equipos.set_model(self.store)
        self.get_equipos()
        
        self.btn_jugadores_cancel = builder.get_object("btn_jugadores_cancel")
        self.btn_jugadores_cancel.connect("clicked", self.on_btn_jugadores_cancel_clicked)
        
        self.btn_jugadores_apply = builder.get_object("btn_jugadores_apply")
        
        if nuevo:
            self.cmbbox_equipos.set_active(0)
            
            self.win_form_jugadores.set_title("Nuevo")
            self.btn_jugadores_apply.connect("clicked", self.on_btn_jugadores_apply_new_clicked)
            self.win_form_jugadores.show_all()
            
        else:
            self.win_form_jugadores.set_title("Editar")
            self.btn_jugadores_apply.connect("clicked", self.on_btn_jugadores_apply_edit_clicked)
            
            selection = self.obj_padre.treeview_jugadores.get_selection()
            model, treeiter = selection.get_selected()
    
            if treeiter is not None:
                self.id_jugador = model[treeiter][0]
                
                db = Database()
                
                sql = """
                    select id_jugador, nom_jugador, ape_jugador, jugador.id_equipo, nom_equipo from jugador 
                    left join equipo on jugador.id_equipo = equipo.id_equipo where id_jugador = %(id_jugador)s
                    """
                
                result = db.run_select_filter(sql, {'id_jugador': self.id_jugador})
                self.entry_nombre_jugador.set_text(result[0][1])
                self.entry_apellidos_jugador.set_text(str(result[0][2]))
                id_equipo = result[0][3]
                
                if id_equipo:
                    index = 0
                    for ite in self.store:
                        if (ite[0] == id_equipo):
                            self.cmbbox_equipos.set_active(index)
                            break
                        index += 1
                else:
                    self.cmbbox_equipos.set_active(0)
                
                self.win_form_jugadores.show_all()
    
    def get_equipos(self):
        db = Database()
        
        sql = """
            select id_equipo, nom_equipo from equipo
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append([0, "Sin equipo"])
            for r in result:
                self.store.append([r[0], r[1]])
    
    def on_btn_jugadores_cancel_clicked(self, button):
        self.win_form_jugadores.hide()
    
    def on_btn_jugadores_apply_new_clicked(self, button):
        nombre = self.entry_nombre_jugador.get_text()
        apellidos = self.entry_apellidos_jugador.get_text()
        
        iterator = self.cmbbox_equipos.get_active_iter()
        if iterator is not None:
            id_equipo = self.store[iterator][0]
            
            if (id_equipo == 0):
                id_equipo = None
        
        db = Database()
        
        sql = """
            insert into jugador (id_equipo, nom_jugador, ape_jugador) values (%(id_equipo)s, %(nom_jugador)s, %(ape_jugador)s)
            """
        db.run_sql(sql, {"id_equipo": id_equipo, "nom_jugador": nombre, "ape_jugador": apellidos})
        
        self.obj_padre.get_jugadores()
        self.win_form_jugadores.hide()
    
    def on_btn_jugadores_apply_edit_clicked(self, button):
        nombre = self.entry_nombre_jugador.get_text()
        apellidos = self.entry_apellidos_jugador.get_text()
        
        iterator = self.cmbbox_equipos.get_active_iter()
        if iterator is not None:
            id_equipo = self.store[iterator][0]
            if (id_equipo == 0):
                id_equipo = None
        
        db = Database()
        
        sql = """
            update jugador set id_equipo = %(id_equipo)s, nom_jugador = %(nom_jugador)s, ape_jugador = %(ape_jugador)s where id_jugador = %(id_jugador)s
            """
        db.run_sql(sql, {"id_jugador": self.id_jugador, "id_equipo": id_equipo, "nom_jugador": nombre, "ape_jugador": apellidos})
        
        self.obj_padre.get_jugadores()
        self.win_form_jugadores.hide()
