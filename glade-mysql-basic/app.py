#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database
from equipos import Equipos, EquiposForm
from jugadores import Jugadores, JugadoresForm
from utils import Mensage

class Main:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("xml/main.glade")
        
        self.win_main = builder.get_object("win_main")
        self.win_main.connect("destroy", self.on_main_window_destroy)
        
        self.btn_equipos = builder.get_object("btn_equipos")
        self.btn_equipos.connect("clicked", self.on_btn_equipos_clicked)
        
        self.btn_jugadores = builder.get_object("btn_jugadores")
        self.btn_jugadores.connect("clicked", self.on_btn_jugadores_clicked)
        
        self.btn_salir = builder.get_object("btn_salir")
        self.btn_salir.connect("clicked", self.on_main_window_destroy)
        
        self.win_main.show_all()
        
        Gtk.main()
    
    def on_btn_equipos_clicked(self, button):
        equipos = Equipos()
    
    def on_btn_jugadores_clicked(self, button):
        jugadores = Jugadores()
        
    def on_main_window_destroy(self, *args):
        Gtk.main_quit()

def main(args):
    db = Database()
    app = Main()
    
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
